package com.chs.modulo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponceModel(
    val `data`: List<Data>,
    val `data2`: List<Data>,
    val statusCode: Int,
    val statusMessage: String
) {
    class Data : Serializable {
        @SerializedName("queue_")
        @Expose
        var queue_: String = ""

    }
}