package com.chs.modulo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RequestModel {
    @SerializedName("batchNumber")
    @Expose
    var batchNumber: String? = ""

}