package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.model.ResponceModel
import kotlinx.android.synthetic.main.adapter_items.view.*

class ItemsAdapter (val mContext: Context?, val queuesList: List<ResponceModel.Data>, val comingFrom: String) : RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_items, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ItemsAdapter.ViewHolder, position: Int) {
        holder.bindItems(queuesList[position], mContext!!, comingFrom)
        holder.itemView.mText.setOnClickListener {
            onCartChangedListener!!.setCartClickListener("zoomImage", position, "")
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return queuesList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mModel: ResponceModel.Data, mContext: Context, comingFrom: String) {

//            Picasso.with(mContext).load(WebServices.imageBaseUrl + mModel.item + ".jpg")
//                .placeholder(R.mipmap.ic_launcher)
//                .into(itemView.itemImage)

            itemView.mText.setText("" + mModel.queue_)
        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int, value: String)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}