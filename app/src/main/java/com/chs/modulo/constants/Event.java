package com.chs.modulo.constants;

/**
 * Created by ANDROID on 6/10/2016.
 */
public interface Event {
    /**
     * int constants for framework dialogs/events/API calls
     */

    int PERMISSION_REQUEST_Location = 1;
    int PERMISSION_REQUEST_File = 2;
    int PERMISSION_REQUEST_PhoneCall = 3;
    int PERMISSION_REQUEST_Camera = 4;
    int ValidateUser = 5;
    int ForgotPassword = 6;
    int ExtraDialodParam = 7;
    int RegisterNewCustomer = 8;

}
