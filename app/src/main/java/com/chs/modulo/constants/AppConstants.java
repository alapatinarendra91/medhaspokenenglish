package com.chs.modulo.constants;

/**
 * constants to be used store values
 *
 * @author Narendra
 */

public class AppConstants {
    /*Key values used in SharedPreferences*/
    public static String gcmRegId = "gcmRegId";
    public static String PREF_NAME = "Modulo_pref";
    public static String auth_token = "auth_token";
    public static String PREF_Username = "PREF_Username";
    public static String PREF_Password = "PREF_Password";
    public static String PREF_RememberMe = "PREF_RememberMe";
    public static String PREF_RegisteredUser = "PREF_RegisteredUser";


    public static String queuesToPullDateFormate = "MM/dd/yy";
    public static String userDateFormate = "MM/dd/yyyy";
    public static String serverDateFormate = "yyyy-MM-dd";


    /*Used For intents only*/
    public static String Intent_ModelClass_Data = "Intent_ModelClass_Data";
    public static String Intent_ComingFrom = "Intent_ComingFrom";
}
