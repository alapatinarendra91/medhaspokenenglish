package com.chs.modulo.ui.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import chs.application.BaseApplication;
import chs.ext.RequestManager;

public class ModuloBaseApplication extends BaseApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.


    public static ModuloBaseApplication instance = null;

    private static Context context;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressWarnings("unused")
    @Override
    public void onCreate() {

        if (false && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().
                    detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().
                    detectAll().penaltyDeath().build());
        }
        super.onCreate();
        instance = this;

//        ModuloBaseApplication.context = getApplicationContext();
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Font-Regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
//
//        /*Intialize the fabric for getting crash reports*/
//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)  // Enables Crashlytics debugger
//                .build();
//        Fabric.with(fabric);

    }


    @Override
    protected void initialize() {

        /***
         * Request manager for volley
         */

        RequestManager.initializeWith(this, new RequestManager.Config("data/data/predento/pics",
                5242880, 4));

    }


    public static Context getAppContext() {
        return ModuloBaseApplication.context;
    }

    public void onTerminate() {
        super.onTerminate();
    }

}

