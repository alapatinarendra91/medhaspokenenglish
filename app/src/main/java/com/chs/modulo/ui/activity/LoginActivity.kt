package com.warehouse.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.AlertDialogHelper
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.ResponceModel
import com.chs.modulo.ui.activity.MainActivity
import com.chs.modulo.ui.activity.ModuloBaseActivity
import com.chs.modulo.ui.activity.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class LoginActivity : ModuloBaseActivity(), View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private var mContext: Context? = null
    private var alertDialog: AlertDialogHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mContext = this
        initViews()

    }

    private fun initViews() {
        setToolBarHeading("Login")

        alertDialog = AlertDialogHelper(mContext)
        alertDialog!!.setOnAlertListener(this)

        loginBtn.setOnClickListener(this)
        forgotPasswordText.setOnClickListener(this)
        newCustomerSignUpBtn.setOnClickListener(this)

        if (PrefUtil.getBool(
                this@LoginActivity,AppConstants.PREF_RememberMe,AppConstants.PREF_NAME)) {
            userNameEdit.setText(PrefUtil.getString(this@LoginActivity,AppConstants.PREF_Username,AppConstants.PREF_NAME))
            passwordEdt.setText(PrefUtil.getString(this@LoginActivity, AppConstants.PREF_Password,AppConstants.PREF_NAME))
            rememberCheck.isChecked = true
            userNameEdit.setSelection(userNameEdit.getText().toString().length)
            passwordEdt.setSelection(passwordEdt.text.toString().length)
        }

    }

    override fun onClick(view: View?) {

        when (view!!.getId()) {
            R.id.loginBtn -> {

                if (validateUi()){
                    val usersArrayList = ArrayList<String>()
                    if (PrefUtil.loadList(mContext, AppConstants.PREF_NAME,AppConstants.PREF_RegisteredUser) != null) {
                        usersArrayList.addAll(PrefUtil.loadList(mContext,AppConstants.PREF_NAME,AppConstants.PREF_RegisteredUser))
                        for (i in 0 until usersArrayList.size){
                           val userName  = CommonUtils.splitString(usersArrayList.get(i), ":", 0)
                           val password  = CommonUtils.splitString(usersArrayList.get(i), ":", 1)
                            if (userName.toLowerCase().equals(userNameEdit.text.toString().toLowerCase())){
                                if (password.toLowerCase().equals(passwordEdt.text.toString().toLowerCase())){
                                    startActivity(Intent(applicationContext, MainActivity::class.java))
                                    return
                                }
                            }
                        }
                    }
                    CommonUtils.showToastLong(mContext, "Please chek your Email/Password")

                }


//                    getData(Event.ValidateUser)

            }

            R.id.forgotPasswordText -> {
                if (userNameEdit.getText().toString() == "") {
                    CommonUtils.showToast(this@LoginActivity, "Please enter Email")
                    return
                }

                alertDialog?.showAlertDialog("Forgot Password","Are you sure you want to get your password to registered email?",
                    "Yes","No",Event.ForgotPassword,true)
            }

            R.id.newCustomerSignUpBtn -> {
                startActivity(Intent(mContext, RegisterActivity::class.java))
            }
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(userNameEdit).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(mContext)))
        return if (mForm.isValid) true else false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        var request = ""

        when (actionID) {
            Event.ValidateUser -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.ValidateUser + userNameEdit.text + "/" + passwordEdt.text,
                    reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.ValidateUser)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.ValidateUser, response)
                    }
                })
            }

            Event.ForgotPassword -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.ForgotPassword + userNameEdit.text,
                    reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.ForgotPassword)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.ForgotPassword, response)
                    }
                })
            }
        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.ValidateUser -> {
                    var mLoginModel = serviceResponse as ResponceModel
                    CommonUtils.showToast(mContext, mLoginModel.statusMessage)
                    PrefUtil.putString(mContext, AppConstants.PREF_Username, userNameEdit.text.toString() ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_Password, passwordEdt.text.toString() ,AppConstants.PREF_NAME)
                    PrefUtil.putBool(mContext, AppConstants.PREF_RememberMe, rememberCheck.isChecked ,AppConstants.PREF_NAME)
                    startActivity(Intent(applicationContext, MainActivity::class.java))

                }
                Event.ForgotPassword -> {
                    var mForgotModel = serviceResponse as ResponceModel
                    alertDialog?.showAlertDialog("Forgot Password",
                        mForgotModel.statusMessage,
                        "Ok",
                        Event.ExtraDialodParam,
                        true
                    )
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    /**
     * This method is invoked when the positive button is clicked
     * @param from
     */
    override fun onPositiveClick(from: Int) {
        if (from == Event.ForgotPassword)
            getData(Event.ForgotPassword)

    }

    /**
     * This method is invoked when the negative button is clicked
     * @param from
     */
    override fun onNegativeClick(from: Int) {

    }

    /**
     * This method is invoked when the neutral button is clicked
     * @param from
     */
    override fun onNeutralClick(from: Int) {

    }

}