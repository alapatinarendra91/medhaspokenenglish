package com.chs.modulo.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.chs.modulo.R
import com.warehouse.ui.activity.LoginActivity

class SplashScreenActivity : ModuloBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splash()

    }

    /**
     * This Method will Show our logo for 3 seconds
     */
   private fun splash() {
        Handler().postDelayed({
            startActivity(Intent(applicationContext, MainActivity::class.java))
//            startActivity(Intent(applicationContext, LoginActivity::class.java))
            finish()
        }, 100)
//        }, 3000)
    }

}