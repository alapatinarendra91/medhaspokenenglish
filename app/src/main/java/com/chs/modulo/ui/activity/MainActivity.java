package com.chs.modulo.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.chs.modulo.HomeFragment;
import com.chs.modulo.R;
import com.chs.modulo.adapter.ExpandableListAdapter;
import com.chs.modulo.model.MenuModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends ModuloBaseActivity
{
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Home", true, false);
        headerList.add(menuModel);

        menuModel = new MenuModel("English", true, true); //Menu of Java Tutorials
        headerList.add(menuModel);

        List<MenuModel> childModelsList = new ArrayList<>();

        MenuModel childModel = new MenuModel("Spoken english-standard", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Spoken english-advanced", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Social english", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Competative english", false, false);
        childModelsList.add(childModel);

        childModel = new MenuModel("Vocabuulary", false, false);
        childModelsList.add(childModel);


        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        headerList.add(new MenuModel("Motivation", true, false));
        headerList.add(new MenuModel("Events", true, false));
        headerList.add(new MenuModel("Statistics", true, false));
        headerList.add(new MenuModel("Downloads", true, false));
        headerList.add(new MenuModel("Share the app", true, false));
        headerList.add(new MenuModel("Contact", true, false));
        headerList.add(new MenuModel("Terms & Conditions", true, false));
        headerList.add(new MenuModel("Logout", true, false));

    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                        onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                        onBackPressed();
                    Toast.makeText(MainActivity.this, "groupPosition: " + groupPosition + "  Chaild pos: " + childPosition, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });
    }
}

