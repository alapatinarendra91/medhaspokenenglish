package com.chs.modulo

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.adapter.ImageAdapter
import com.chs.modulo.ui.activity.MainActivity
import com.chs.modulo.ui.activity.PlayVideoExampleActivity
import com.chs.modulo.ui.fragment.ModuloBaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import me.relex.circleindicator.CircleIndicator
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.chs.modulo.adapter.VideoAdapter
import java.util.*
import androidx.recyclerview.widget.RecyclerView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : ModuloBaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var rootView: View? = null
    private var mContext: Context? = null
    private var indicator: CircleIndicator? = null
    private lateinit var linearLayoutManager: LinearLayoutManager
    var mAdapter: VideoAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_home, container, false)

        rootView = inflater.inflate(R.layout.fragment_home, null, false)
        mContext = activity
//        setToolBarHeading("Home")

        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)
        if (timeOfDay >= 0 && timeOfDay < 12) {
            rootView!!.wishText.setText("Good Morning").toString()
        } else if (timeOfDay >= 12 && timeOfDay < 21) {
            rootView!!.wishText.setText("Good Afternoon").toString()
        }
//        else if (timeOfDay >= 16 && timeOfDay < 21) {
//            rootView!!. wishText.setText("Good Evening").toString()
////            Toast.makeText(this, "Good Evening", Toast.LENGTH_SHORT).show()
//        }
        else if (timeOfDay >= 21 && timeOfDay < 24) {
            rootView!!.wishText.setText("Good Evening").toString()
        }



        rootView!!.aboutApp.setOnClickListener(View.OnClickListener {
            startActivity(Intent(mContext, PlayVideoExampleActivity::class.java))
        })


        indicator = rootView!!.findViewById(R.id.indicator) as CircleIndicator
//        indicator.setRadius(4 * resources.displayMetrics.density)

        val mAdapter = ImageAdapter(mContext)
        rootView!!.mViewPager.adapter = mAdapter
        indicator!!.setViewPager(rootView!!.mViewPager)

        rootView!!.mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    sliderView_1.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                    sliderView_2.setBackgroundColor(resources.getColor(R.color.gray))
                    sliderView_3.setBackgroundColor(resources.getColor(R.color.gray))
                }
                if (position == 1) {
                    sliderView_1.setBackgroundColor(resources.getColor(R.color.gray))
                    sliderView_2.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                    sliderView_3.setBackgroundColor(resources.getColor(R.color.gray))
                }
                if (position == 2) {
                    sliderView_1.setBackgroundColor(resources.getColor(R.color.gray))
                    sliderView_2.setBackgroundColor(resources.getColor(R.color.gray))
                    sliderView_3.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })


//        linearLayoutManager = LinearLayoutManager(this.mContext)
//        videoRecyclerView.layoutManager = linearLayoutManager
//        //adding some dummy data to the list
//               //creating our adapter
//        val adapter = VideoAdapter()
//
//        //now adding the adapter to recyclerview
//        videoRecyclerView.adapter = adapter
//        var mainMenu = rootView!!.findViewById(R.id.videoRecyclerView) as RecyclerView
//        mainMenu.layoutManager = LinearLayoutManager(context,
//            LinearLayoutManager.VERTICAL, false)
//        mainMenu.adapter = VideoAdapter()
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
