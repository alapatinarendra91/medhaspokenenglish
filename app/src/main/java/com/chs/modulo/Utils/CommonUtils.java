package com.chs.modulo.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.chs.modulo.R;
import com.chs.modulo.constants.AppConstants;
import com.chs.modulo.constants.Event;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TimeZone;
import java.util.TreeMap;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Calibrage10 on 10/27/2016.
 */
public class CommonUtils<T>  {
    private static NotificationManager manager;
    private static boolean alertType = false;
    public static boolean isShowNotifiction = false;
    private static Date convertedDate;
    private static String outputDateStr;
    private static boolean flag = true;
    public static String StoragePath = Environment.getExternalStorageDirectory().getPath() + "/YIS-Warehouse";
    private static String fileUrl, fileName, nameOFfile;
    private static boolean viewPdfVar;
    public static int fileSize;
    private static Context mContext;


    /**
     * Getting current date from calender
     *
     * @param format
     * @return
     */
    public static String getcurrentDate(String format) {
        Calendar c = Calendar.getInstance();
        String date;
        SimpleDateFormat Objdateformat = new SimpleDateFormat(format);
        date = Objdateformat.format(c.getTime());
        return date;
    }

    public static  String getCurrentDateFromTimeZone(String timeZone,String outPutFormate){
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat outputFmt = new SimpleDateFormat(outPutFormate);
        outputFmt.setTimeZone(TimeZone.getTimeZone(timeZone));
        return outputFmt.format(time);
    }

    public static  String getCurrentTimeFromTimeZone(String timeZone,String outPutFormate){
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat outputFmt = new SimpleDateFormat(outPutFormate);
        outputFmt.setTimeZone(TimeZone.getTimeZone(timeZone));
        return outputFmt.format(time);
    }

    /*Get the logined User name from shared prefarenses*/
    public static  String getUserId(Context mContext){
        return PrefUtil.getString(mContext, AppConstants.PREF_Username, AppConstants.PREF_NAME);
    }

    /**
     * @param dateFormat
     * @param days
     * @return
     */
    public static String getFuture_OR_PastDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    /*
     * future Time returned negative Value
     * Past Time return positive value
     * if value equals to At prasent return 0
     * */

    public static int compareTime(String starttime, String endtime,String pattern) {

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(starttime);
            Date date2 = sdf.parse(endtime);

            if(date1.equals(date2) ) {
                return 0;
            }else if(date1.before(date2) ) {
                return -1;
            } else {

                return 1;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * @param ctx
     * @return
     */
    public static boolean isNetworkAvailable(Context ctx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Converting bitmap to byte array
     *
     * @param byteArray
     * @return
     */
    public static Bitmap getBitmapFromByteArray(String byteArray) {
        byte[] decodedString = Base64.decode(byteArray, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    /**
     * @param context
     * @param notificationId
     * @param title
     * @param message
     */
    public static void showProgressNotification(Context context, int notificationId, String title, String message) {
        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "0");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel;
                mChannel = new NotificationChannel
                        ("0", title, NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription(message);
                mChannel.enableVibration(true);
                manager.createNotificationChannel(mChannel);
        }

        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setTicker("")
                .setOngoing(true)
                .setProgress(100, FileDownloader.percentageValue, false);
        manager.notify(notificationId, mBuilder.build());
    }

    /**
     * @param context
     * @param id
     */
    public static void removeProgressNotification(Context context, int id) {
        manager.cancel(id);
    }

    /**
     * @param mContext
     * @param mSpinner
     * @param name
     * @return
     */
    public static boolean checkSpinnerPosition(Context mContext, Spinner mSpinner, String name) {
        if (mSpinner.getSelectedItemPosition() <= 0) {
            showToast(mContext, "Please select " + name);
            return false;
        }

        return true;
    }

    /**
     * @param context
     * @param url
     * @param name
     * @param viewVar
     */
    public static void download(Context context, String url, String name, boolean viewVar) {
        viewPdfVar = viewVar;
        mContext = context;
        fileUrl = url;
        nameOFfile = name;
        fileName = name.replace(" ", "");
        new DownloadFile().execute(url, fileName);
    }

    /**
     *
     */
    private static class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... strings) {
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "Download");
            folder.mkdir();
            File pdfFile = new File(folder, fileName);

            try {

                if (!pdfFile.exists())
                    pdfFile.createNewFile();
                else {
                    pdfFile.createNewFile();
                    if (viewPdfVar)
                        isShowNotifiction = true;
                    view();
                    if (pdfFile.exists()) {
                        showServiceToast("Download completed..Please check downloads folder in your mobile");
                        //removeProgressNotification(mContext, 1);
                    }


                    return null;
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile, fileSize, mContext, nameOFfile);

            if (viewPdfVar)
                view();
            else {
                showServiceToast("Download completed..Please check downloads folder in your mobile");
                // removeProgressNotification(mContext, 1);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    /**
     * @param message
     */
    public static void showServiceToast(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * Download pdf and view the pdf
     */
    public static void view() {
//        try {
        Uri path;
        // removeProgressNotification(mContext, 1);
        File pdfFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + fileName);  // -> filename = maven.pdf
//        File pdfFile = new File("file:///storage/emulated/0/YIS-Warehouse/1203201911064.0.pdf");  // -> filename = maven.pdf
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            path = Uri.fromFile(pdfFile);
        else
            path = FileProvider.getUriForFile(mContext, "com.warehouseprovider.provider", pdfFile);

        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            pdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            mContext.startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            showServiceToast("No Application available to view PDF");

        }
    }


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    public static Spanned formateHtml(String header, String value) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(("<b><font color=\"#464544\">" + header + "</font></b>" + value), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(("<b><font color=\"#464544\">" + header + "</font></b>" + value));
        }
        return result;
    }

    public static Spanned formateHtmlAsBigSmall(String header, String value) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(("<b><font color=\"#464544\">" + header + "</font></b>" + value), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(("<b><font color=\"#464544\">" + header + "</font></b>" + value));
        }
        return result;
    }

    /**
     * @param mContext
     * @param subject
     * @param messageBody
     */
    public static void shareIntentData(Context mContext, String subject, String messageBody) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, messageBody);
        mContext.startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

    /**
     * @param plainTxt
     * @return
     */
    public static String EncryptData(String plainTxt) {
        String base64_encode = "";
        byte[] data = new byte[0];
        try {
            data = plainTxt.getBytes("UTF-8");
            base64_encode = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "" + base64_encode;
    }

    /**
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param context
     * @param message
     */
    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    /**
     * @param mContext
     * @return
     */
    public static boolean isGPSEnabled(Context mContext) {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param mContext
     * @param requestType
     */
    public static void requestPermission(Context mContext, int requestType) {

        if (requestType == Event.PERMISSION_REQUEST_Location) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.ACCESS_FINE_LOCATION))
                Toast.makeText(mContext, "Location permission allows us to take location. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            else
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Event.PERMISSION_REQUEST_Location);

        } else if (requestType == Event.PERMISSION_REQUEST_File) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(mContext, "file permission allows us to access ExternalData. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Event.PERMISSION_REQUEST_File);
            }

        } else if (requestType == Event.PERMISSION_REQUEST_Camera) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.CAMERA)) {
                Toast.makeText(mContext, "CAMERA permission allows us to capture images. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CAMERA}, Event.PERMISSION_REQUEST_Camera);
            }
        } else if (requestType == Event.PERMISSION_REQUEST_PhoneCall) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.CALL_PHONE)) {
                Toast.makeText(mContext, "CALL_PHONE permission allows us to make calls. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CALL_PHONE}, Event.PERMISSION_REQUEST_PhoneCall);
            }
        }

    }

    /**
     * @param mContext
     * @param xml
     * @param isFullscreen
     * @param isOpenKeyboard
     * @return
     */
    public static Dialog dialogInitialize(Context mContext, int xml, boolean isFullscreen, boolean isOpenKeyboard) {
        Dialog dialog;
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dialog.setContentView(xml);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        Double width = metrics.widthPixels * .95;
        Double height = metrics.heightPixels * .74;
        Window win = dialog.getWindow();
        win.setLayout(width.intValue(), height.intValue());
        if (isFullscreen)
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        dialog.getWindow().setSoftInputMode(isOpenKeyboard ? WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE : WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return dialog;
    }


    public static void loadImage(Context mContext, ImageView mImage, String url){
        Picasso.with(mContext).load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(mImage);

    }

    /**
     * @param d1
     * @param d2
     * @return
     */
    public static String compareDoubleValues(double d1, double d2) {
        // compares the two specified double values
        int retval = Double.compare(d1, d2);
        if (retval > 0) {
            System.out.println("d1 is greater than d2");
            return "greater";
        } else if (retval < 0) {
            System.out.println("d1 is less than d2");
            return "less";
        } else {
            System.out.println("d1 is equal to d2");
            return "equal";
        }
    }

    /**
     * @param formattedString
     * @return
     */
    public static String mobileNoFormat(String formattedString) {
        if (formattedString == null || formattedString.toUpperCase().equalsIgnoreCase("NULL"))
            return "";
        if (formattedString.length() < 10)
            return formattedString;

        return "(" + formattedString.substring(0, 3) + ") "
                + formattedString.substring(3, 6) + "-" + formattedString.substring(6, 10);
    }

    /**
     * @param tv
     * @return
     */
    public static boolean isValidMobile(EditText tv) {
        boolean check = false;
        if (!tv.getText().toString().contains("(")) {
            check = false;
            tv.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>"));
            tv.requestFocus();
            return check;
        }

        String phone2 = tv.getText().toString().replace("-", "").replace(" ", "").replace("(", "").replace(")", "");
        if (phone2.length() < 10 || phone2.length() > 10) {
            check = false;
            tv.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>"));
            tv.requestFocus();

        } else {
            check = true;
        }

        return check;
    }

    /**
     * @param context
     * @param list
     * @return
     */
    public static ArrayAdapter<String> setAdapter(Context context, ArrayList<String> list) {
        ArrayAdapter<String> adapterSet = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        adapterSet.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapterSet;
    }

    /**
     * @param mSpinner
     * @param list
     * @param item
     */
    public static void spinnerBindItem(Spinner mSpinner, ArrayList list, String item) {
        if (item == null) {
            mSpinner.setSelection(0);
            return;
        }

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).toString().contains(item)) {
                mSpinner.setSelection(i);
                break;
            }
        }
    }

    /**
     * @param list
     * @param value_str
     * @return
     */
    public static int adapterPosition(ArrayList<String> list, String value_str) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(value_str)) {
                return i;
            }
        }

        return 0;
    }

    /**
     *
     */
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    /**
     * @param value
     * @return
     */
    public static String format(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        final NumberFormat numberFormat = new DecimalFormat("#####");
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return (hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix);
    }

    /**
     * @param value
     * @return
     */
    public static String getRoundValue(String value) {
        try {

            if (value == null || value.toUpperCase().equals("NULL") || Double.parseDouble(value) == 0)
                return "0";

            if (Double.parseDouble(value) < 1)
                return "" + value;

//            return "" + NumberFormat.getInstance().format(Math.round(Double.parseDouble(value)));
              return NumberFormat.getNumberInstance(Locale.US).format(Math.round(Double.parseDouble(value)));
        }catch (Exception e){
            return "0";
        }

    }

    /**
     * @param value
     * @return
     */
    public static String getNextValue(String value) {
        if (value == null || value.toUpperCase().equals("NULL"))
            return "$0";
        if (Double.parseDouble(value) < 1)
            return "$" + value;

        return "$" + (int) Math.ceil(Double.parseDouble(value));

    }

    /**
     * @param value
     * @return
     */

    public static String showTwoDecimals(String value) {
        if (value == null || value.trim().equalsIgnoreCase("null") || value.trim().equalsIgnoreCase("") || Double.parseDouble(value) == 0)
            return "0";

        Double valueDouble = Double.parseDouble(new DecimalFormat("##.00").format(Double.valueOf(value)));

        /*if value in B/W  0 to -0.99 */
        if (valueDouble < 0 && valueDouble > Double.parseDouble("-0.99")) {
            return "0" + new DecimalFormat("##.00").format(Double.valueOf(value));
        }
        /*if value bellow 0 only(Negative value)*/
        if (valueDouble < Double.parseDouble("0")) {
            return new DecimalFormat("##.00").format(Double.valueOf(value));
        }

         /*if value Above 0 and bellow 1 only */
        if (valueDouble < Double.parseDouble("1")) {
            return "0" + new DecimalFormat("##.00").format(Double.valueOf(value));
        }

//        return "" + new DecimalFormat("##.00").format(Double.valueOf(value));
//        return "" + new DecimalFormat("#,###.00").format(Double.valueOf(value));
        return "" + new DecimalFormat("#,###,###.00").format(Double.valueOf(value));
    }

    /**
     * @param value
     * @return
     */
    public static String showSingleDecimals(String value) {
        if (value == null || value.trim().equalsIgnoreCase("") || Double.parseDouble(value) == 0)
            return "0";

        /*if value bellow 1 only*/
        if (Double.parseDouble(new DecimalFormat("##.0").format(Double.valueOf(value))) < Double.parseDouble("1")) {
            return "0" + new DecimalFormat("##.0").format(Double.valueOf(value));
        }
        return "" + new DecimalFormat("##.0").format(Double.valueOf(value));
    }

    /**
     * @param value
     * @return round_decimal_to_nearest_integer
     */

    public static String round_decimal_to_nearest_integer(String value) {

        try {
            if (value == null || value.toUpperCase().equals("NULL"))
                return "0";
            if (Double.parseDouble(value) > 0 && Double.parseDouble(value) < 1)
                return "" + "0"+value;

            return "" + (int) Math.ceil(Double.parseDouble(value));
        }catch (Exception e){
            return "0";
        }

    }

    public static String replaceNegetiveValueAsBracketsForClaims(String value){
        if (value == null || value.toUpperCase().equals("NULL")|| value.equals(""))
            return "0";

        if (Double.parseDouble(value) < 0){
            return "(" + showTwoDecimals(value).replace("-", "") + ")";
        }


        return showTwoDecimals(value);
    }

    /**
     * @param value
     * @return
     */
    public static String getFormattedFloatValue(String value) {
        if (value == null || value.toUpperCase().equals("NULL")|| value.equals(""))
            return "0";

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        return format.format(Float.parseFloat(value));

    }

    /**
     * @param value
     * @return
     */
    public static String getFormattedThousands(String value) {
        if (value == null || value.equals(""))
            return "0";

        return showSingleDecimals("" + ((Double.valueOf(value)) / 1000)) + "K";
    }

    /**
     * @param inputDateStr
     * @return
     */
    public static String dateFormat(String inputDateStr) {
        if (inputDateStr == null || inputDateStr.equals(""))
            return "";

        DateFormat inputFormat = new SimpleDateFormat(inputDateStr.contains("-") ? "yyyy-MM-dd" : "MM/dd/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
        String strCurrentDate = inputDateStr;
        try {
            if (strCurrentDate != null) {
                convertedDate = inputFormat.parse(strCurrentDate);
                outputDateStr = outputFormat.format(convertedDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDateStr;

    }

    /**
     * @param inputDateFormate
     * @param outputDateFormate
     * @param inputDateStr
     * @return
     */
    public static String changeDateFormat(String inputDateFormate, String outputDateFormate, String inputDateStr) {
        if (inputDateStr == null || inputDateStr.equals(""))
            return "";

        DateFormat inputFormat = new SimpleDateFormat(inputDateFormate);
        DateFormat outputFormat = new SimpleDateFormat(outputDateFormate);
        String strCurrentDate = inputDateStr;
        try {
            if (strCurrentDate != null) {
                convertedDate = inputFormat.parse(strCurrentDate);
                outputDateStr = outputFormat.format(convertedDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDateStr;
    }

    /*
    * future date returned negative Value
    * Past date return positive value
    * if value equals to today return 0
    * */
    public static long dateDifference(String endDate) {
        if (endDate == null || endDate.length() < 10)
            return 0;

        Calendar today_date = Calendar.getInstance();

        Calendar pre_date = Calendar.getInstance();
        pre_date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(endDate.substring(3, 5)));
        pre_date.set(Calendar.MONTH, Integer.parseInt(endDate.substring(0, 2)) - 1); // 0-11 so 1 less
        pre_date.set(Calendar.YEAR, Integer.parseInt(endDate.substring(6, 10)));

        long diff = today_date.getTimeInMillis() - pre_date.getTimeInMillis(); //result in millis
        long days = diff / (24 * 60 * 60 * 1000);
//        logPrint("TAG", "diff is " + days);
        return days;

    }

    /*
     * future date returned negative Value (Like -1,-2,-3)
     * Past date return positive value (Like 1,2,3)
     * if value equals to today return 0
     * */
    public static long dateDifference(String startDate,String endDate) {
        if (endDate == null || endDate.length() < 10 || startDate == null || startDate.length() < 10)
            return 0;

        Calendar today_date = Calendar.getInstance();
        today_date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(startDate.substring(3, 5)));
        today_date.set(Calendar.MONTH, Integer.parseInt(startDate.substring(0, 2)) - 1); // 0-11 so 1 less
        today_date.set(Calendar.YEAR, Integer.parseInt(startDate.substring(6, 10)));

        Calendar pre_date = Calendar.getInstance();
        pre_date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(endDate.substring(3, 5)));
        pre_date.set(Calendar.MONTH, Integer.parseInt(endDate.substring(0, 2)) - 1); // 0-11 so 1 less
        pre_date.set(Calendar.YEAR, Integer.parseInt(endDate.substring(6, 10)));

        long diff = today_date.getTimeInMillis() - pre_date.getTimeInMillis(); //result in millis
        long days = diff / (24 * 60 * 60 * 1000);
//        Log.e("TAG", "diff is " + days);

        return days;

    }

    /**
     * @param startDate
     * @param endDate
     * @return
     */
    public static int compareDates(String startDate, String endDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date1 = sdf.parse(startDate);
            Date date2 = sdf.parse(endDate);

            if (date1.compareTo(date2) > 0) {
                logPrint("app", "Date1 is after Date2");
                return -1;
            } else if (date1.compareTo(date2) < 0) {
                logPrint("app", "Date1 is before Date2");
                return 1;
            } else if (date1.compareTo(date2) == 0) {
                logPrint("app", "Date1 is equal to Date2");
                return 0;
            }
        } catch (Exception e) {
            CommonUtils.logPrint("app", "Exception is : " + e.toString());
        }

        return 0;
    }

    /**
     * @param mContext
     * @param mIntent
     * @return
     */
    public static boolean isPackageManagerIntentNull(Context mContext, Intent mIntent) {
        PackageManager packageManager = mContext.getPackageManager();
        if (mIntent.resolveActivity(packageManager) != null) {
            return true;
        }

        return false;
    }

    /**
     * @param map
     * @param value
     * @return
     */
    public static String getKeyFromValue(LinkedHashMap<String, String> map, String value) {
        String toReturnValue = "";
        for (Map.Entry entry : map.entrySet()) {
            if (value.equalsIgnoreCase(entry.getValue().toString())) {
                toReturnValue = (String) entry.getKey();
                break; //breaking because its one to one map
            }
        }
        return toReturnValue;
    }

    /**
     * @param editText
     */
    public static void editTextPhoneFormat(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String str = editText.getText().toString();
                if (str.length() == 10 && flag) {
                    editText.setText("(" + str.substring(0, 3) + ") " + str.substring(3, 6) + "-" + str.substring(6, 10));
                    editText.setSelection(editText.getText().toString().length());
                    flag = false;
                }

                if (str.length() == 9 && flag == false) {
                    flag = true;
                    editText.setText(editText.getText().toString().replace(" ", "").replace("(", "").replace(")", ""));
                    editText.setSelection(editText.getText().toString().length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    /**
     * @param inputSpinner
     * @return
     */
    public static boolean isEmptySpinner(final Spinner inputSpinner) {
        if (null == inputSpinner) return true;
        if (inputSpinner.getSelectedItemPosition() == -1 || inputSpinner.getSelectedItemPosition() == 0) {
            return true;
        }
        return false;
    }

    /**
     * @param mContext
     * @return
     */
    public static boolean isTablet(Context mContext) {
        return mContext.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * @param mEtSearch
     * @param context
     */
    public static void showKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    /**
     *
     * @param message
     */
    public static void logPrint(String message) {
        logPrint("tag", message);
    }

    /**
     * @param tag
     * @param message
     */
    public static void logPrint(String tag, String message) {
        Log.e(tag == null || tag.trim().length() == 0 ? "tag" : tag, "" + message);
    }

    public static <T>Double getDiscount(T param1, T param2){
        Double persantage = 0.0;
//        if (param1 == null || param2 == null)
//            return null;

        try {
            persantage = 100 - (Double.parseDouble(""+ param1) / Double.parseDouble(""+param2)) * 100;
        }catch (Exception e){
            logPrint("" + e.getMessage());
        }

        return persantage;
    }

    public static <T> void isShowDiscountPrice(T view,String discount,String bindValue){
        try {
            /* Any view*/
            if (discount == null || discount.toUpperCase().equals("NULL") || discount.equals("")){
                View tv = (View)view;
                tv.setVisibility(GONE);
                return;
            }

            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                if (Double.parseDouble(discount) > 0){
                    tv.setText(bindValue);
                    tv.setVisibility(VISIBLE);
                }else
                    tv.setVisibility(GONE);

            }else  {
                View tv = (View) view;
                tv.setVisibility(Double.parseDouble(discount) > 0 ? VISIBLE : GONE);
            }

        }catch (Exception e){
            View tv = (View)view;
            tv.setVisibility(GONE);
        }

    }

    /**
     * @param first
     * @param second
     * @return
     */
    public static String subtractNumbers(String first, String second) {
        Float result = 0.0f;
        try {
            if (first != null && !first.equals("null"))
                result = Float.parseFloat(first);

            if (second != null && !second.equals("null"))
                result = result - Float.parseFloat(second);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "" + result;

    }

    /**
     * @param priceTxt
     * @param StrikeOut_priceStr
     * @param displayStr
     */
    public static void showPrice(TextView priceTxt, String StrikeOut_priceStr, String displayStr) {
        try {

            if (StrikeOut_priceStr == null || displayStr == null || StrikeOut_priceStr.toUpperCase().equals("NULL") ||displayStr.toUpperCase().equals("NULL") || Double.parseDouble(showTwoDecimals(displayStr).replace(",", "")) >= Double.parseDouble((showTwoDecimals(StrikeOut_priceStr).replace(",", "")))) {
                priceTxt.setVisibility(GONE);
            } else {
                priceTxt.setVisibility(VISIBLE);
                priceTxt.setText("$" + showTwoDecimals(StrikeOut_priceStr));
                priceTxt.setPaintFlags(priceTxt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

        }catch (Exception e){
            priceTxt.setVisibility(GONE);
        }

    }

    /**
     * @param input
     * @param splitChar
     * @param position
     * @return
     */
    public static String splitString(String input, String splitChar, int position) {
        if (input == null)
            return "";

        String[] splitStr = input.split(splitChar);
        if (splitStr.length <= position)
            return "";

        return splitStr[position];
    }


    public static String splitStringAsTwoOnly(String input, String splitChar, int position) {
        if (input == null || input.trim().equals(""))
            return "";

        String fName = "";
        String lName = "";

        try {

        String[] arr = input.split(splitChar);
        for (int i = 0; i < arr.length; i++) {
            if (i == 0)
                fName = arr[i];
            else
                lName = lName + splitChar + arr[i];
        }

        return position == 0 ? fName : lName.substring(1);

        }catch (Exception e){
            return "";
        }
    }

    /**
     * @param dp
     * @param mContext
     * @return
     */
    public static float dpToPx(Context mContext, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());
    }

    /**
     * @param path
     * @return
     */
    public static Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }

    /**
     * Sting Split with space
     *
     * @param inputString
     * @return
     */
    public static String splitWithSpace(String inputString) {
        String fName = "", lName = "";

        String UserFullName = inputString;
        String[] arr = UserFullName.split(" ");
        for (int i = 0; i < arr.length; i++) {
            if (i == 0)
                fName = arr[i];
            else
                lName = lName + " " + arr[i];
        }
        return arr[1];
    }

    /**
     * @param first
     * @param second
     * @return
     */
    public static String subtractedNumbers(String first, String second) {
        Float result = 0.0f;
        try {
            if (first != null && !first.equals("null"))
                result = Float.parseFloat(first);

            if (second != null && !second.equals("null"))
                result = result - Float.parseFloat(second);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "" + result;

    }


    /**
     * @param first
     * @param second
     * @return
     */
    public static Float addNumbers_float(String first, String second) {
        Float result = 0.0f;
        try {
            if (first != null && !first.equals("null"))
                result = Float.parseFloat(first);

            if (second != null && !second.equals("null"))
                result = result + Float.parseFloat(second);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    /**
     * @param first
     * @param second
     * @return
     */
    public static Integer addNumbers_Integer(String first, String second) {
        Float result = 0.0f;
        try {
            if (first != null && !first.equals("null"))
                result = Float.parseFloat(first);

            if (second != null && !second.equals("null"))
                result = result + Float.parseFloat(second);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return Integer.parseInt(getRoundValue("" + result));

    }

    /**
     * Open link in browser
     *
     * @param mContext
     * @param link
     */
    public static void openLinkInBrowser(Context mContext, String link) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(link));
            mContext.startActivity(i);
        } catch (Exception e) {
            showToast(mContext, "issue with URL");
        }

    }

    /**
     * @param mContext
     * @param Number
     */
    public static void makeCall(Context mContext, String Number) {
        /*Checking is device have SimSlot or Not*/
        if (isPackageManagerIntentNull(mContext, new Intent(Intent.ACTION_DIAL)))
            mContext.startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + Number)));
    }


    public static void sendMail(Context mContext, String customerEMailAddress,String subject) {
        Intent repEmailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + customerEMailAddress));
        repEmailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        mContext.startActivity(Intent.createChooser(repEmailIntent, "Chooser Title"));
    }

    public static void openFile(Context mContext, File url) {

        try {

//            Uri uri = Uri.fromFile(url);
            Uri uri;
            Intent intent = new Intent(Intent.ACTION_VIEW);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                uri = Uri.fromFile(url);
            else{
                uri = FileProvider.getUriForFile(mContext, "com.warehouseprovider.provider", url);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }


            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")){
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            showToast(mContext, "No application found which can open the file");
        }
    }

}
