package com.chs.modulo.Utils;

/**
 * Created by ANDROID on 01/12/2016.
 */
import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.chs.modulo.Utils.CommonUtils.showProgressNotification;

/*Download any file from URL*/
public class FileDownloader {
    private static final int  MEGABYTE = 1024 * 1024;
    public   static int percentageValue ;


    /**
     * Download file from URL
     * @param fileUrl
     * @param directory
     * @param fileSize
     * @param mContext
     * @param name
     */
    public static void downloadFile(String fileUrl, File directory, int fileSize, Context mContext, String name){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            fileSize = totalSize;

            CommonUtils.logPrint("", "totalSize is: " + totalSize);

            long total = 0;

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;

            CommonUtils.logPrint("", "buffer is: " + buffer);


            while((bufferLength = inputStream.read(buffer))>0 ){
                total += bufferLength;
                percentageValue = (int)((total*100)/totalSize);
                fileOutputStream.write(buffer, 0, bufferLength);
                CommonUtils.isShowNotifiction = false;
                showProgressNotification(mContext, 1, name, "File downloading");
                CommonUtils.logPrint("", "percentageValue is: " + percentageValue);
            }

            fileOutputStream.close();
            CommonUtils.removeProgressNotification(mContext, 1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
