package com.chs.modulo.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ANDROID on 8/1/2016.
 */

/*
* Utility class to store/retrieve values in SharedPreferences.
* */
public class PrefUtil {

    /**
     *
     * @param context
     * @param key
     * @param value
     * @param pref
     */
    public static void putString(Context context, String key, String value, String pref) {
        if(context != null && key != null) {
            if(pref != null && !pref.isEmpty()) {
                context.getSharedPreferences(pref, 0).edit().putString(key, value).apply();
            } else {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
            }

        }
    }

    /**
     *
     * @param context
     * @param key
     * @param pref
     * @return
     */
    public static String getString(Context context, String key, String pref) {
        return context != null && key != null?(pref != null && !pref.isEmpty()?context.getSharedPreferences(pref, 0).getString(key, (String)null): PreferenceManager.getDefaultSharedPreferences(context).getString(key, (String)null)):null;
    }

    /**
     *
     * @param context
     * @param key
     * @param value
     * @param pref
     */
    public static void putInt(Context context, String key, int value, String pref) {
        if (context==null || key == null) { return; }
        if (pref==null || pref.isEmpty()) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
        } else {
            context.getSharedPreferences(pref, Context.MODE_PRIVATE).edit().putInt(key, value).apply();
        }
    }

    /**
     *
     * @param context
     * @param key
     * @param pref
     * @return
     */
    public static int getInt(Context context, String key, String pref) {
        if (context==null || key == null) { return 0; }
        if (pref==null || pref.isEmpty()) {
            return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, 0);
        } else {
            return context.getSharedPreferences(pref, Context.MODE_PRIVATE).getInt(key, 0);
        }
    }

    /**
     *
     * @param context
     * @param key
     * @param value
     * @param pref
     */
    public static void putBool(Context context, String key, boolean value, String pref) {
        if (context==null || key == null) { return; }
        if (pref==null || pref.isEmpty()) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
        } else {
            context.getSharedPreferences(pref, Context.MODE_PRIVATE).edit().putBoolean(key, value).apply();
        }
    }

    /**
     *
     * @param context
     * @param key
     * @param pref
     * @return
     */
    public static boolean getBool(Context context, String key, String pref) {
        if (context==null || key == null) { return false; }
        if (pref==null || pref.isEmpty()) {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
        } else {
            return context.getSharedPreferences(pref, Context.MODE_PRIVATE).getBoolean(key, false);
        }
    }

    /**
     *
     * @param context
     * @param pref
     */
    public static void clearAllSharedPrefarences(Context context, String pref){
        context.getSharedPreferences(pref, 0).edit().clear().apply();
    }

    /**
     *
     * @param context
     * @param pref
     * @param key
     */
    public static void clearSharedPrefarence(Context context, String pref, String key){
        context.getSharedPreferences(pref, 0).edit().remove(key).commit();
    }

    /**
     *
     * @param context
     * @param pref_name
     * @param key
     * @param countries
     */
    public static void storeList(Context context, String pref_name, String key, List countries) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(countries);
        editor.putString(key, jsonFavorites);
        editor.apply();
    }

    /**
     *
     * @param context
     * @param pref_name
     * @param key
     * @return
     */
    public static ArrayList<String> loadList(Context context, String pref_name, String key) {

        SharedPreferences settings;
        List<String> favorites;
        settings = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        if (settings.contains(key)) {
            String jsonFavorites = settings.getString(key, null);
            Gson gson = new Gson();
            String[] favoriteItems = gson.fromJson(jsonFavorites, String[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<>(favorites);
        } else
            return null;
        return (ArrayList<String>) favorites;
    }

    /**
     *
     * @param context
     * @param pref_name
     * @param key
     * @param searchItem
     */
    public static void addList(Context context, String pref_name, String key, String searchItem) {
        List<String> favorites = loadList(context, pref_name, key);
        if (favorites == null)
            favorites = new ArrayList<>();

        if(favorites.contains(searchItem)){
            favorites.remove(searchItem);
        }
        favorites.add(searchItem);

        if(favorites.size() > 8) {
            favorites.remove(0);
        }

        storeList(context, pref_name, key, favorites);
    }



}


