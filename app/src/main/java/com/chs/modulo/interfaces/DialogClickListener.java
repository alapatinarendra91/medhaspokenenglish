package com.chs.modulo.interfaces;

/**
 * Created by Calibrage10 on 9/5/2017.
 */

/*Created for testing purpose*/

public interface DialogClickListener {
    public void onYesClick();
    public void onNoClick();
}