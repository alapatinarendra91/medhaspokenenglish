package com.chs.modulo.interfaces;

/**
 * Created by Calibrage10 on 8/21/2017.
 */

/*
* Declared all API's and called using interface
* */
public interface WebServices {

    // Local URL
    String BASE_URL = "http://192.168.20.45/YISWarehouseAppService/";

    // Test URL
//    String BASE_URL = "https://www.youngsinc.com/YISCUSTAPPTEST_SERVICE/";

    // Beta URL
//    String BASE_URL = "https://www.youngsinc.com/YIS_APP_CUST_SERVICES_PRO/Beta/";

    /*Live URL*/
//    String BASE_URL = "https://www.youngsinc.com/YIS_APP_CUST_SERVICES_PRO/V4/";




    String imageBaseUrl = "https://www.youngsinc.com/images/";


    String ValidateUser = BASE_URL + "queues/ValidateUser/"; // Sp nsp_app_Scanman_ValidateUser
    String ForgotPassword = BASE_URL + "queues/ForgotPassword/"; // Sp nsp_app_Scanman_ValidateUser
    String RegisterNewCustomer = BASE_URL + "queues/RegisterNewCustomer/"; // Sp nsp_app_Scanman_ValidateUser


}
