/*
 *
 *  Proprietary and confidential. Property of Calibrage info systems. Do not disclose or distribute.
 *  You must have written permission from Calibrage info systems. to use this code.
 *
 */

package chs.gcm;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import androidx.legacy.content.WakefulBroadcastReceiver;

public abstract class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
    }

    /**
     * param context
     *
     * @return the component name. FeaturedCollectionResponse //ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
     */

    public abstract ComponentName getComponentName(Context context);

}

	
