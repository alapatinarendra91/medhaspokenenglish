package chs.ext;

/**
 * Created by Narendra.Alapati.
 */
//public class CollectionDeserializer implements JsonDeserializer<Collection<?>> {
//
//    @Override
//    public Collection<?> deserialize(JsonElement json, Type typeOfT,
//                                     JsonDeserializationContext context) throws JsonParseException {
//        Type realType = ((ParameterizedType)typeOfT).getActualTypeArguments()[0];
//
//        return parseAsArrayList(json, realType);
//    }
//
//    /**
//     *
//     * @param type
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//    public <T> ArrayList<T> parseAsArrayList(JsonElement json, T type) {
//        ArrayList<T> newArray = new ArrayList<T>();
//        Gson gson = new Gson();
//
//        JsonArray array= json.getAsJsonArray();
//        Iterator<JsonElement> iterator = array.iterator();
//
//        while(iterator.hasNext()){
//            JsonElement json2 = (JsonElement)iterator.next();
//            T object = (T) gson.fromJson(json2, (Class<?>)type);
//            newArray.add(object);
//        }
//
//        return newArray;
//    }
//
//}
