/*
 *
 *  Proprietary and confidential. Property of Calibrage info systems. Do not disclose or distribute.
 *  You must have written permission from Calibrage info systems. to use this code.
 *
 */

package chs.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.multidex.BuildConfig;

import com.chs.modulo.R;
import com.chs.modulo.Utils.CommonUtils;

import chs.utils.FontUtils;


/**
 * @author Narendra
 */
public class CustomTextView extends TextView {

    private static String LOG_TAG = "CustomTextView";

    /**
     * @param context
     */
    public CustomTextView(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    /**
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs) {
        int[] viewsAllAttrIdsArr = R.styleable.com_yiscustomer_ui_widget_CustomTextView;
        int fontAttributeId = R.styleable.com_yiscustomer_ui_widget_CustomTextView_fontAssetName;
        boolean isFontSet = FontUtils.setCustomFont(this, context, attrs, viewsAllAttrIdsArr, fontAttributeId);
        if (!isFontSet && BuildConfig.DEBUG) {
            CommonUtils.logPrint(LOG_TAG, "Failed to set custom font.");
        }
    }
}
